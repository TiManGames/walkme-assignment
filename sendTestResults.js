require("dotenv").config();

var fs = require("fs");
var path = require("path");

let mailgun = require("mailgun-js")({
	apiKey: process.env.API_KEY,
	domain: process.env.DOMAIN,
});

var jsonPath = path.join(
	path.resolve(__dirname),
	"cypress",
	"report",
	"mochawesome-report"
);

var file = fs.readdirSync(jsonPath).filter((fn) => fn.endsWith(".json"));
var filePath = __dirname + "/cypress/report/mochawesome-report/" + file;

let data = {
	from: "Excited User <me@samples.mailgun.org>",
	to: "idan@zest.is",
	subject: "Cypress Test Results - WalkMe",
	text: "Cypress test results for WalkMe demo page.",
	attachment: filePath,
};

mailgun.messages().send(data, (err, body) => {
	console.log(body);
});
