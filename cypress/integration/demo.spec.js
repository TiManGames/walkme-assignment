/// <reference types="cypress" />

describe("WalkMe request demo tests", () => {
  context("Iphone-11 pro", () => {
    beforeEach(() => {
      //cy.viewport(2436, 1125);
    });

    it("Should open request demo form - Request a Demo", () => {
      cy.get("a")
        .contains("Request a Demo")
        .first()
        .should("be.visible")
        .parent()
        .parent()
        .focus()
        .click();
      cy.get("form[name='New Form']").should("be.visible");
    });

    it("Should open request demo form - Get Workstation", () => {
      cy.get("a")
        .contains("Get Workstation")
        .should("be.visible")
        .scrollIntoView()
        .parent()
        .parent()
        .focus()
        .click();
      cy.get("form[name='New Form']").should("be.visible");
    });
  });
});
